# 实验九报告——矩阵乘法单核优化探索

2023311I28 崔俊杰

## 背景

前几个实验中的openmp,pthresh,MPI等对矩阵乘法的优化都是建立在并行计算的  
基础上，这种优化的原理主要是通过让矩阵的不同块可以同时进行计算，而不需要  
等到循环遍历到再进行计算，可以在一定程度上提高矩阵乘法的效率  
**而这种效率的提升上限在于对应的cpu的核数等硬性因素，且这种效率提升我认为是常数优化**  
这种优化会随着矩阵规模的扩大而效率下降，这显然不是我们希望看到的  
那么对单核矩阵乘法算法上的优化就显得格外重要了

## 优化方式总览

(1) 寄存器优化  
(2) SIMD向量化优化  
(3) ijk顺序优化，即循环遍历优化  
(4) 消除指针别名优化  
(5) 矩阵分块优化  

## 优化效果展示

### 一，navie_MMult朴素暴力

```cpp
void MY_MMult(int m, int n, int k, double *A, int lda,
               double *B, int ldb,
               double *C, int ldc)
{
    for (int i = 0; i < m; i++){
        for (int j = 0; j < n; j++){
            for (int p = 0; p < k; p++){
                C[i * n + j] += A[i * k + p]*B[p * n + j];
            }
        }
    }
}
```

|矩阵大小|gflops|
|-------|------|
|8|1.024|
|16|0.910222|
|32|0.936229|
|64|0.893165|
|128|0.880601|
|256|0.903409|
|512|0.639531|
|1024|0.630277|
|2048|0.138305|

### 二，naive_MMult_optimize1寄存器优化

```cpp
// + 寄存器优化
void MY_MMult(int m, int n, int k, double *A, int lda,
               double *B, int ldb,
               double *C, int ldc)
{
    for (register int i = 0; i < m; i++){
        for (register int j = 0; j < n; j++){
            register double temp_c = 0;
            for (register int p = 0; p < k; p++){
                temp_c += A[i * k + p]*B[p * n + j];
            }
            C[i * n + j] += temp_c;
        }
    }
}
```

原理：分析发现循环过程中有些变量经常被访问使用，而每次都是通过数据线在内存空间去读写这些数据，效率不如通过寄存器去访问它们，于是把i,j,p这些循环变量放入寄存器中去优化

|矩阵大小|gflops|
|-------|------|
|8|1.024|
|16|1.6384|
|32|2.11407|
|64|2.04003|
|128|2.20521|
|256|1.94012|
|512|0.931641|
|1024|0.881436|
|2048|0.176958|

**分析：矩阵规模小的时候提升较大，几倍，但是大了就几乎无提升了**

### 三，naive_MMult_optimize2向量化编译

添加编译选项：-O3 -fomit-frame-pointer -march=x86-64 -ffast-math 即可自动向量化

|矩阵大小|gflops|
|-------|------|
|8|1.024|
|16|8.192|
|32|9.36229|
|64|10.0825|
|128|3.94944|
|256|3.69095|
|512|1.04533|
|1024|1.01491|
|2048|0.193491|

**分析：也有提升，但不显著**

### 四，naive_MMult_optimize3循环优化

**原理解释**：

对应i,j,p的遍历顺序，一共有6种排列情况，显然下面这种是最快的

```cpp
// + 循环优化(内存寻址优化)
void MY_MMult(int m, int n, int k, double *A, int lda,
               double *B, int ldb,
               double *C, int ldc)
{
    for (register int i = 0; i < m; i++){
        for (register int p = 0; p < k; p++){
            register double temp_a = A[i * k + p];
            for (register int j = 0; j < n; j++){
                C[i * n + j] += temp_a * B[p * n + j];
            }
        }
    }
}
```
因为最内存循环的A,B,C三个数组的偏移地址都是连续变化的，大大提高了访存的效率

|矩阵大小|gflops|
|-------|------|
|8|1.024|
|16|8.192|
|32|5.95782|
|64|9.53251|
|128|8.63026|
|256|12.2105|
|512|9.79263|
|1024|9.27735|
|2048|4.55715|

**分析：效率提升较显著**

### 五，naive_MMult_optimize4消除指针别名优化

**原理解释：将A，B，C绑定为访存过程的唯一名字的指针**

具体实现，double * A 改为double * __restrict__ A，其他同理

|矩阵大小|gflops|
|-------|------|
|8|1.024|
|16|8.192|
|32|13.1072|
|64|15.4202|
|128|14.7687|
|256|14.8405|
|512|14.2884|
|1024|13.487|
|2048|5.97738|

**分析：效率提升显著**

### 六，naive_MMult_optimize5矩阵分块优化

```cpp
// + 分块优化
void cut_block(int br,int bc,int block,int m,int n,int k,double * __restrict__ A,
				double * __restrict__ B,
				double * __restrict__ C)
{
	for (register int i = br; i < br + block; i++){
        for (register int p = 0; p < k; p++){
            register double temp_a = A[i * k + p];
            for (register int j = bc; j < bc + block; j++){
                C[i * n + j] += temp_a * B[p * n + j];
            }
        }
    }
}
void MY_MMult(int m, int n, int k, double * __restrict__ A, int lda,
               double * __restrict__ B, int ldb,
               double * __restrict__ C, int ldc)
{
	register int block = 8;
	for (register int i = 0; i < m; i += block){
		for (register int j = 0; j < n; j += block){
			cut_block(i,j,block,m,n,k,A,B,C);
		}
	}
}
```

|矩阵大小|gflops|
|-------|------|
|8|1.024|
|16|8.192|
|32|13.1072|
|64|11.6508|
|128|11.4598|
|256|9.42276|
|512|6.45247|
|1024|5.6554|
|2048|1.39662|

**分析：加了分块反而效率下降，是因为矩阵规模较小，未能体现，具体原因可参考：https://blog.csdn.net/weixin_40673608/article/details/88135041**

## 总效果图对比

![alt text]({986FE46F-8094-466E-BD9C-19461763E34D}.png)


