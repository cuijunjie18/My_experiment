import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 15})

fig, ax = plt.subplots(figsize=(15, 10))
x = [8, 16, 32, 64, 128, 256, 512, 1024, 2048]
gflops_naive  = [1.024,0.910222,0.936229,0.893165,0.880601,0.903409,0.639531,0.630277,0.138305]
gflops_1 = [1.024,1.6384,2.11407,2.04003,2.20521,1.94012,0.931641,0.881436,0.176958 ]
gflops_2 = [1.024,8.192,9.36229,10.0825,3.94944,3.69095,1.04533,1.01491,0.193491]
gflops_3  = [1.024,8.192,5.95782,9.53251,8.63026,12.2105,9.79263,9.27735,4.55715]
gflops_4 = [1.024,8.192,13.1072,15.4202,14.7687,14.8405,14.2884,13.487,5.97738]
gflops_5 = [1.024,8.192,13.1072,11.6508,11.4598,9.42276,6.45247,5.6554,1.39662]

plt.plot(x, gflops_naive, label='naive', color='blue', linestyle='-', linewidth=3.0)
plt.plot(x, gflops_1, label='1', color='yellow', linestyle='-', linewidth=3.0)
plt.plot(x, gflops_2, label='2', color='purple', linestyle='-', linewidth=3.0)
plt.plot(x, gflops_3,label='3', color='green', linestyle='-', linewidth=3.0)
plt.plot(x, gflops_4,label='4', color='black', linestyle='-', linewidth=3.0)
plt.plot(x, gflops_5,label='5', color='red', linestyle='-', linewidth=3.0)


plt.title('dgemm_optimize')
plt.xlabel('matrix size')
plt.ylabel('gflops')
plt.legend()

# plt.savefig('result.png')
plt.show()