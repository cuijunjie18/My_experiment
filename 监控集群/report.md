# 遇到的问题

## 一，prometheus.service启动失败
产生原因：下载的版本为**prometheus-3.0.0-beta.0.linux-amd64**，版本过高，与wsl2下的Ubuntu版本不兼容

解决方案：降低prometheus的版本为**prometheus-2.55.0-rc.0.linux-amd64**

## 二，自己写的shell脚本无法运行
产生原因：.sh脚本文件缺少执行权限

解决方案：终端执行指令 chmod +x .sh

## 三，shell脚本无法开机自启，即无法自动化部署
解决方案：生成service后缀文件，并执行指令将服务配置为开机自启  
**sudo systemctl enable my_prometheus_update.service**

```service
[Unit]
Description=My Service
After=network.target

[Service]
ExecStart=/home/cjj/hpc/experiment_9_task6/make_yml.sh
WorkingDirectory=/home/cjj/hpc/experiment_9_task6/

[Install]
WantedBy=default.target
```