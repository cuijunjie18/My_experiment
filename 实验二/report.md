# 实验二实验报告

## 基本信息

课程名：高性能计算实验  
学期：2024-2025上半学期  
姓名：崔俊杰

## 实验环境

OS版本：Linux  

gcc版本：gcc version 11.4.0 (Ubuntu 11.4.0-1ubuntu1~22.04)

**cpu信息**：  
（1）型号：12th Gen Intel(R) Core(TM) i7-12700H  
（2）频率：2687.991  
（2）物理核数：10  

内存大小：7993720KB

## 一，test_cblas_dgemm.c实验

**列主序与行主序**  

![alt text](image.png)

结论：计算结果显然是一致的，但是运行的时间有区别，具体原因待查找

## 二，time_dgemm.c中新增naive_dgemm实现，对比实验

初始运行结果：

![alt text](image-1.png)

矩阵计算时间：
![alt text](image-2.png)

### 追加自己的矩阵计算函数(代码在time_dgemm.c文件)(附录也有代码)

（1）**小数据范围比对结果，判断自己的naive_dgemm是否正确**：

![alt text](image-3.png)

结论：**结果正确**

（2）**1024x1024大小比较效率**

![alt text](image-4.png)

**结论：显然lopenblas库的计算效率远大于自己的朴素矩阵计算**  

（3）**一般逐级比较(naive_dgemm计算超过1024大小的矩阵过慢，所以缩小范围)**

| 类别 | 128 | 256 | 512 | 1024 |
| --- | --- | --- | --- | --- |
| cblas_dgemm duration (s)| 0.005307 | 0.021924 | 0.004404 | 0.008968 |
| naive_dgemm duration (s)| 0.009237 | 0.103450 | 0.456181 | 3.570039 |
| cblas_dgemm gflops | 1.580669 | 3.060977 | 121.905293 | 478.921420 |
| naive_dgemm gflops | 0.908153 | 0.648708 | 1.176881 | 3.570039 |

**结论**：显然随着矩阵大小的增加，cblas的计算效率远大于naive  
**疑惑**：为什么随着矩阵增大，**cblas的512大小的计算效率大于256大小的呢？**  
**猜测**：当矩阵大于某个阈值的时候，cblas中的计算方法切换了

## 三，碰到的问题

主要是Linux系统下使用vscode一直打不开，显示Permission denied!  
最后发现是当前用户下，code.exe 文件没有读的权限，故打不开，利用  
指令sudo chmod 744 ./code.exe 修改权限即可

## 附录

### time_dgemm.c代码参考

```c
#include "stdio.h"
#include "stdlib.h"
#include "sys/time.h"
#include "time.h"
#include <cblas.h>

// 编译 gcc -o time_dgemm time_dgemm.c –lopenblas
// 运行 ./time_dgemm 1024
int main(int argc, char *argv[])
{
  if (argc < 2)
  {
    printf("Input Error\n");
    return 1;
  }

  printf("test!\n");
  int i, m, n, k;
  m = n = k = atoi(argv[1]); // n,m,k均相等

  int sizeofa = m * k;
  int sizeofb = k * n;
  int sizeofc = m * n;
  int lda = m;
  int ldb = k;
  int ldc = m;

  double alpha = 1.2;
  double beta = 0.001;

  struct timeval start, finish;
  double duration;

  double *A = (double *)malloc(sizeof(double) * sizeofa);
  double *B = (double *)malloc(sizeof(double) * sizeofb);
  double *C = (double *)malloc(sizeof(double) * sizeofc);
  srand((unsigned)time(NULL));

  for (i = 0; i < sizeofa; i++)
  {
    A[i] = i % 3 + 1; // (rand() % 100) / 100.0;
  }

  for (i = 0; i < sizeofb; i++)
  {
    B[i] = i % 3 + 1; //(rand()%100)/10.0;
  }

  for (i = 0; i < sizeofc; i++)
  {
    C[i] = 0.1;
  }
  /*
  for (int i = 0; i < sizeofa; i++) printf("%f ",A[i]);
  printf("\n");
  for (int i = 0; i < sizeofa; i++) printf("%f ",B[i]);
  printf("\n");
  for (int i = 0; i < sizeofa; i++) printf("%f ",C[i]);
  printf("\n");*/

  printf("m=%d,n=%d,k=%d,alpha=%lf,beta=%lf,sizeofc=%d\n", m, n, k, alpha, beta, sizeofc);
  gettimeofday(&start, NULL);
  cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc);
  gettimeofday(&finish, NULL);

  // 转成成秒数
  duration = (double)(finish.tv_sec - start.tv_sec) + (double)(finish.tv_usec - start.tv_usec) / 1.0e6;
  double gflops = 4.0 * m * n * k;
  gflops = gflops / duration * 1.0e-9;

  FILE *fp;
  fp = fopen("timeDGEMM.txt", "a"); // 追加写
  fprintf(fp, "%dx%dx%d\t%lf s\t%lf GFLOPS\n", m, n, k, duration, gflops);
  fclose(fp);

  // 将标准库计算的C矩阵结果进行存储
  double *D = (double *)malloc(sizeof(double) * sizeofc);
  for (i = 0; i < sizeofc; i++) D[i] = C[i]; // 存储结果
  for (i = 0; i < sizeofc; i++) C[i] = 0.1; // 重置C数组，便于重新计算


  gettimeofday(&start, NULL); // 开始计算时间

  // naive的计算方法，三重循环即可
  for (int i = 0; i < m; i++)
  {
    for (int j = 0; j < n; j++)
    {
      C[i*m + j] = beta*C[i*m + j];
      for (int  p = 0; p < k; p++)
      {
        C[i*m + j] += A[i*k + p]*B[p*n + j];;
      }
    }
  }
  gettimeofday(&finish, NULL);
  // 转成成秒数
  duration = (double)(finish.tv_sec - start.tv_sec) + (double)(finish.tv_usec - start.tv_usec) / 1.0e6;
  gflops = 4.0 * m * n * k;
  gflops = gflops / duration * 1.0e-9;

  fp = fopen("timeDGEMM.txt", "a"); // 追加写
  fprintf(fp, "%dx%dx%d\t%lf s\t%lf GFLOPS\n", m, n, k, duration, gflops);
  fclose(fp);

  /* 输出对比结果
  for (int i = 0; i < sizeofc; i++) C[i] *= alpha; 
  for (int i = 0; i < sizeofc; i++) printf("%f ",D[i]);
  printf("\n");
  for (int i = 0; i < sizeofc; i++) printf("%f ",C[i]);
  */


  free(A);
  free(B);
  free(C);
  free(D);
  return 0;
}

```

### 逐级比较的实际图片展示

![alt text](image-5.png)  
