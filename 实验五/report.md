# 实验五报告

## 一，查看CPU核数

![alt text](image.png)

物理核数 = $ 1 \times 10  = 10 $ 个

## 二，测试创建双线程是否提高了CPU利用率

![alt text](image-3.png)

结论：确实提高了！

## 三，测试单线程矩阵乘法的正确性

分两块
![alt text](image-1.png)

结果如下:
![alt text](image-2.png)

**结论：结果正确**

## 四，测试双线程矩阵乘法的正确性

![alt text](image-4.png)

分两块结果正确；

![alt text](image-5.png)

分四块结果也正确；

## 五，基于1024大小的矩阵测试单线程与多线程(8线程)的效率

结果如下：

![alt text](image-6.png)

结论：单线程与多线程的结果误差均近似为0，且多线程的gflops大于单线程的

## 六，测试大规模矩阵下多线程的情况

### CPU 利用率

![alt text](image-8.png)

理论值：800%
实测值：800%

### 多线程pstree

![alt text](image-7.png)