# 实验三总结

## 一：如何判断可执行文件调用的是哪个版本的MY_MMult函数

MMULT1中多了这一行输出代码
```c
fprintf(stderr, __FILE__ "\n");
```

所以预估运行可执行程序的时候可以看出区别

宏的配置矩阵大小
```c
#define PFIRST 128
#define PLAST 1024
```

**调用的是MMULT0.c时的结果**
![alt text](image-1.png)

**调用的是MMULT1.c时的结果**
![alt text](image-2.png)

结论：符合预期结果，输出了4次(**128开始倍增至1024**)文件名

## 二：一的成功调用同名的不同版本的MY_MMult函数的原因

**有makefile的代码决定**

```makefile
NEW = MMult0 # 这里决定了不同的函数

OBJS  := $(BUILD_DIR)/util.o $(BUILD_DIR)/REF_MMult.o $(BUILD_DIR)/test_MMult.o $(BUILD_DIR)/$(NEW).o
```

## 三：MMULT0 与 MMULT0的对比

![alt text](image.png)

**因为都是naive实现，故性能指标几乎一致**

下面加入优化矩阵乘法，及openblas库

修改的makefile部分：
```makefile
$(BUILD_DIR)/test_MMult.x: $(OBJS) defs.h
	$(LINKER) $(OBJS) $(LDFLAGS) -lopenblas -o$@ # 即多连接了一个外部的openblas的库
```

运行结果：
![alt text](image-4.png)

**结论：显然比naive的gflops值高了几乎两个数量级**

## 四，不同矩阵乘法算法的gflops值的比较

![alt text](image-3.png)

## 五，makefile将数据记录到.m文件的原理

利用了c语言的重定向功能，将输出位置由终端转到了.m文件